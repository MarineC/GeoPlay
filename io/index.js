import http from 'http'
import socketIO from 'socket.io'
import setupMultiplayer from '../server/setupMultiplayerSockets'
import setupSingleplayer from '../server/setupSingleplayerSockets'
import postgresStore from '../server/postgres-store.js'
import config from '../server/server.config'
export default function () {
  this.nuxt.hook('render:before', async (renderer) => {
    const server = http.createServer(this.nuxt.renderer.app)
    const io = socketIO(server)
    setupSingleplayer(io)
    setupMultiplayer(io)
    if (process.env.IN_GITLAB === false) {
      await postgresStore.init(config.postgres)
      try {
        const queryString = 'SELECT * FROM USERS'
        await postgresStore.client.query({
          text: queryString
        })
      } catch (e) {
        e.message += ' \ndid you try to initialize the database with \'npm run db \'?'
        throw e
      }
    }
    // overwrite nuxt.server.listen()
    this.nuxt.server.listen = (port, host) => new Promise(resolve => server.listen(port || process.env.PORT || 3000, process.env.HOST || 'localhost', resolve))
    // close this server on 'close' event
    this.nuxt.hook('close', () => new Promise(server.close))
  })
}
