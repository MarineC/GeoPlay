/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../server/pre.js')
import postgresStore from '../../server/postgres-store.js'


describe('DELETE /api/questions/:id', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return informations of deleted question', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizAndQuestion(agent)
    return agent
      .delete('/api/questions/' + user1.quizzes.questions[0].id_question)
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].question_tag).to.equal(user1.quizzes.questions[0].question_tag)
        expect(response.body[0].response_location_id).to.equal(user1.quizzes.questions[0].response_location_id)
        expect(response.body[0].duration).to.equal(user1.quizzes.questions[0].duration)
        expect(response.body[0].id_quiz).to.equal(user1.quizzes.id_quiz)
      })
  })
  it('should return 404 error if id doesn\'t exist', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizAndQuestion(agent)
    return agent
      .delete('/api/questions/' + 9999)
      .expect(404)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuizAndQuestion()
    return agent
      .delete('/api/questions/' + user1.quizzes.questions[0].id_questionn)
      .expect(401)
  })
})