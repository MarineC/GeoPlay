import apiRouter from '../../routes/routes.js'
import express from 'express'
import request, { agent } from 'supertest'
import session from 'express-session'
import config from '../../server.config.js'
import postgresStore from '../../postgres-store.js'
import User from '../../models/user.model.js'
import Quiz from '../../models/quiz.model.js'
import Score from '../../models/score.model.js'
import Question from '../../models/question.model.js'
const db = require('../../manage-db.js')

class Pre {
    // Generate random strign
    static randomString() {
        return Math.random().toString(36).substr(7)
    }
    static async clearDB () {
      await postgresStore.init(config.postgres)
      await db.deleteTablesContent()
    }

    // Create express
    static createExpress () {
        const app = express()
        app.use(session({
          secret: 'testing',
          resave: false,
          saveUninitialized: false
        }))
        app.use(express.json())
        app.use(express.urlencoded({ extended: false }))
        app.use('/api/', apiRouter)
      
        const agent = request.agent(app)
        return agent
      }

      //login a user
      static async loginUser(agent, userName, password) {
        await agent
        .post('/api/users/login')
        .send({
          username: userName,
          password: password
        })
      }

      // create user
      static async createUser() {
        const rString = Pre.randomString()
        const user = await User.addUser(
            'Fakeuser' + rString,
            'pwd'
        )
        user.password = 'pwd'
        return user
      }

      // create user & login
      static async createUserLogged(agent) {
        const rString = Pre.randomString()
        const user = await User.addUser(
            'Fakeuser' + rString,
            'pwd'
        )
        user.password = 'pwd'
        await Pre.loginUser(agent, user.username, user.password)
        return user
      }

      // create basic quiz without question
      static async createQuiz(id_user = 1) {
        const rString = Pre.randomString()
        const quiz = await Quiz.addQuiz(
            'Fake Quiz ' + rString,
            'Fake Description',
            'Fake mapId',
            'Fake difficulty',
            25,
            id_user,
            5
        )
        return quiz
      }

      // create multiple basic quizzes
      static async createQuizzes(id_user = 1) {
        const quizzes = []
        for (let i = 0; i < 2; i++) {
          const rString = Pre.randomString()
          const quiz = await Quiz.addQuiz(
              'Fake Quiz ' + rString,
              'Fake Description',
              'Fake mapId',
              'Fake difficulty',
              25,
              id_user,
              5
          )
          quizzes.push(quiz)
        }
        return quizzes
      }

      // create quiz with one question
      static async createQuizWithQuestion(id_user = 1) {
        const rString = Pre.randomString()
        const quiz = await Quiz.addQuiz(
            'Fake Quiz ' + rString,
            'Fake Description',
            'Fake mapId',
            'Fake difficulty',
            25,
            id_user,
            5
        )
        quiz.questions = await Pre.createQuestion(quiz.id_quiz)
        return quiz
      }

      // create a question
      static async createQuestion(quizId = 1) {
        const rString = Pre.randomString()
        const question = await Question.addQuestions(
          quizId,
          [{
            question_tag: 'fake tag ' + rString,
            response_location_id: 'fake response location id ' + rString,
            duration: 5
          }]
        )
        question.response_location_id = 'fake response location id ' + rString
        return question
      }

       // create many questions
       static async createQuestions(quizId = 1) {
        const questions = []
        for (let i = 0; i < 2; i++) {
          const rString = Pre.randomString()
          const question = await Question.addQuestions(
            quizId,
            [{
              question_tag: 'fake tag ' + rString,
              response_location_id: 'fake response location id ' + rString,
              duration: 5
            }]
          )
          question.response_location_id = 'fake response location id ' + rString
          questions.push(question)
        }
        return questions
      }

      // create a score 
      static async createScore(id_quiz = 1, id_user = 1) {
        const score = await Score.addScore(
            5,
            id_quiz,
            id_user
        )
        return score
      }

      // create multiple scores
      static async createScores(id_quiz = 1, id_user = 1) {
        const scores = []
        for (let i = 0; i < 2; i++) {
          const score = await Score.addScore(
            5,
            id_quiz,
            id_user
        )
          scores.push(score)
        }
        return scores
      }

      // create a user with one quiz
      static async createUserWithQuiz () {
        const user = await Pre.createUser()
        user.quizzes = await Pre.createQuiz(user.id_user)
        return user
      }
      
      // create a user with multiple quizzes
      static async createUserWithQuizzes () {
        const user = await Pre.createUser()
        user.quizzes = await Pre.createQuizzes(user.id_user)
        return user
      }

      // create a user logged with one quiz
      static async createUserLoggedWithQuiz (agent) {
        const user = await Pre.createUserLogged(agent)
        user.quizzes = await Pre.createQuiz(user.id_user)
        return user
      }

      // create a user logged with multiple quizzes
      static async createUserLoggedWithQuizzes (agent) {
        const user = await Pre.createUserLogged(agent)
        user.quizzes = await Pre.createQuizzes(user.id_user)
        return user
      }

      // create a user with quiz & score
      static async createUserWithQuizAndScore () {
        const user = await Pre.createUser()
        user.quizzes = await Pre.createQuiz(user.id_user)
        user.scores = await Pre.createScore(user.quizzes.id_quiz, user.id_user)
        return user
      }

      // create a user logged with quiz & score
      static async createUserLoggedWithQuizAndScore (agent) {
        const user = await Pre.createUserLogged(agent)
        user.quizzes = await Pre.createQuiz(user.id_user)
        user.scores = await Pre.createScore(user.quizzes.id_quiz, user.id_user)
        return user
      }

      // create a user with quiz & many scores
      static async createUserWithQuizAndScores () {
        const user = await Pre.createUser()
        user.quizzes = await Pre.createQuiz(user.id_user)
        user.scores = await Pre.createScores(user.quizzes.id_quiz, user.id_user)
        return user
      }

      // create a user logged with quiz & many scores
      static async createUserLoggedWithQuizAndScores (agent) {
        const user = await Pre.createUserLogged(agent)
        user.quizzes = await Pre.createQuiz(user.id_user)
        user.scores = await Pre.createScores(user.quizzes.id_quiz, user.id_user)
        return user
      }
      
      // create user logged with one quiz & one question
      static async createUserWithQuizAndQuestion () {
        const user = await Pre.createUser()
        user.quizzes = await Pre.createQuizWithQuestion(user.id_user)
        return user
      }
      
      // create user logged with one quiz & one question
      static async createUserLoggedWithQuizAndQuestion (agent) {
        const user = await Pre.createUserLogged(agent)
        user.quizzes = await Pre.createQuizWithQuestion(user.id_user)
        return user
      }
}

module.exports = Pre