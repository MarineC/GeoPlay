/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('POST /api/questions/add', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return questions informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .post('/api/questions/add')
      .send({
          quizId: user.quizzes.id_quiz,
          questionList: [
            {
              question_tag: 'fake tag',
              response_location_id: 'fake response location id',
              duration: 5
            }
          ]
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].id_question).to.be.an('number')
        expect(response.body[0].question_tag).to.equal('fake tag')
        expect(response.body[0].response_location_id).to.equal('fake response location id')
        expect(response.body[0].duration).to.equal(5)
        expect(response.body[0].id_quiz).to.equal(user.quizzes.id_quiz)
      })
  })
  it('should return 500 error if wrong data', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .post('/api/questions/add')
      .send({
          quizId: user.quizzes.id_quiz,
          questionList: [
            {
              question_tag: 'fake tag',
              response_location_id: 'fake response location id',
              duration: 'wrong data'
            }
          ]
      })
      .expect(500)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserWithQuiz()
    return agent
      .post('/api/questions/add')
      .send({
          quizId: user.quizzes.id_quiz,
          questionList: [
            {
              question_tag: 'fake tag',
              response_location_id: 'fake response location id',
              duration: 5
            }
          ]
      })
      .expect(401)
  })
})