/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('PUT /api/questions/:questionId', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())
  it('should modify question and return the new informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuizAndQuestion(agent)
    const newQuestionTag = 'new question tag'
    const newResponseLocId = 'new response location id'
    const newDuration = 6
    return agent
      .put('/api/questions/' + user.quizzes.questions[0].id_question)
      .send({
        newQuestionTag: newQuestionTag,
        newResponseLocId: newResponseLocId,
        newDuration: newDuration,
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].question_tag).to.equal(newQuestionTag)
        expect(response.body[0].response_location_id).to.equal(newResponseLocId)
        expect(response.body[0].duration).to.equal(newDuration)
      })
  })
  it('should return 404 error if wrong id', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuizAndQuestion(agent)
    const newQuestionTag = 'new question tag'
    const newResponseLocId = 'new response location id'
    const newDuration = 6
    return agent
      .put('/api/questions/' + user.quizzes.questions[0].id_question)
      .send({
        newQuestionTag: newQuestionTag,
        newResponseLocId: newResponseLocId,
        newDuration: 'wrong data',
      })
      .expect(404)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserWithQuizAndQuestion()
    const newQuestionTag = 'new question tag'
    const newResponseLocId = 'new response location id'
    const newDuration = 6
    return agent
      .put('/api/questions/' + user.quizzes.questions[0].id_question)
      .send({
        newQuestionTag: newQuestionTag,
        newResponseLocId: newResponseLocId,
        newDuration: newDuration,
      })
      .expect(401)
  })
})