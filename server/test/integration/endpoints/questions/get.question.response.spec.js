/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('GET /api/questions/response/:questionId', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return response for a specific question', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuizAndQuestion(agent)
    return agent
    .get('/api/questions/response/' + user.quizzes.questions[0].id_question)
    .expect(200)
    .expect('Content-Type', /json/)
    .then((response) => {
      expect(response.body[0].response_location_id).to.equal(user.quizzes.questions.response_location_id)
    })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserWithQuizAndQuestion()
    return agent
    .get('/api/questions/response/' + user.quizzes.questions[0].id_question)
    .expect(401)
  })
})