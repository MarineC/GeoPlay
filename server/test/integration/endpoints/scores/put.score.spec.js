/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('PUT /api/scores/:scoreId', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())
  it('should modify scores and return the new scores informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuizAndScore(agent)
    const newScore = 6
    return agent
      .put('/api/scores/' + user.scores.id_score)
      .send({
        newScore: newScore,
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].id_score).to.equal(user.scores.id_score)
        expect(response.body[0].score_value).to.equal(6)
      })
  })
  it('should return an 404 error if tou send wrong data', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuizAndScore(agent)
    return agent
      .put('/api/scores/' + user.scores.id_score)
      .send({
        newScore: 'wrong data',
      })
      .expect(404)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserWithQuizAndScore()
    const newScore = 6
    return agent
      .put('/api/scores/' +  user.scores.score_id)
      .send({
        newScore: newScore,
      })
      .expect(401)
  })
})