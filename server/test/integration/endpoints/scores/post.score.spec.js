/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('POST /api/scores/add', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return quiz informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .post('/api/scores/add')
      .send({
          quizId: user.quizzes.id_quiz,
          userId: user.id_user,
          score_value: 6
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_score).to.be.a('number')
        expect(response.body.score_value).to.equal(6)
        expect(response.body.id_user).to.equal(user.id_user)
        expect(response.body.id_quiz).to.equal(user.quizzes.id_quiz)
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserWithQuiz()
    return agent
      .post('/api/scores/add')
      .send({
        quizId: user.quizzes.id_quiz,
        userId: user.id_user,
        score_value: 6
      })
      .expect(401)
  })
})