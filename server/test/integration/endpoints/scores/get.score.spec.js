/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('GET /api/scores/:id', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return scores value and username for one quiz', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizAndScores(agent)
    return agent
      .get('/api/scores/' + user1.quizzes.id_quiz)
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(2)
        expect(response.body[0].score_value).to.equal(user1.scores[0].score_value)
        expect(response.body[0].username).to.equal(user1.username)
        expect(response.body[1].score_value).to.equal(user1.scores[1].score_value)
        expect(response.body[1].username).to.equal(user1.username)
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuizAndScores()
    return agent
      .get('/api/quizzes/' + user1.quizzes.id_quiz)
      .expect(401)
  })
})