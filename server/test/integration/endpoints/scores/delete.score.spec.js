/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('DELETE /api/scores/:id', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return scores value and username for one quiz', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizAndScores(agent)
    return agent
      .delete('/api/scores/' + user1.scores[0].id_score)
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].id_score).to.equal(user1.scores[0].id_score)
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuizAndScores()
    return agent
      .delete('/api/scores/' + user1.scores[0].id_score)
      .expect(401)
  })
})