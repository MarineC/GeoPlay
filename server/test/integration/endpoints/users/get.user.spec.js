/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'

describe('GET /api/users/:id', () => {

  before(async () => {
      await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return username of user', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .get('/api/users/' + user1.id_user)
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.username).to.equal(user1.username)
      })
  })
  it('should return 404 error', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .get('/api/users/' + 99999)
      .expect(404)
  })
  it('should return 401 error', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUser()
    return agent
      .get('/api/users/' + user1.id_user)
      .expect(401)
  })
})