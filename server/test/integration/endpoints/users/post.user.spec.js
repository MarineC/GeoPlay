/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('POST /api/users/add', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return username and id of new user', async () => {
    const agent = await Pre.createExpress()
    return agent
      .post('/api/users/add')
      .send({
          username: 'fakeUser',
          password: 'fakePwd'
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_user).to.be.a('number')
        expect(response.body.username).to.equal('fakeUser')
      })
  })
  it('should an error message "This username is already taken" if new user choose the an usermane that already exist', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUser()
    return agent
      .post('/api/users/add')
      .send({
          username: user1.username,
          password: 'fakePwd'
      })
      .expect(409)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.equal('This username is already taken')
      })
  })
})