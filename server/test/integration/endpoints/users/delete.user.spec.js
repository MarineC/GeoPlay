/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'

describe('DELETE /api/users/:userId', () => {

  before(async () => {
      await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return username and id of deleted user', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .delete('/api/users/' + user1.id_user)
      .send({
          password: user1.password
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_user).to.equal(user1.id_user)  
        expect(response.body.username).to.equal(user1.username)
      })
  })
  it('should return 403 if the password is wrong', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .delete('/api/users/' + user1.id_user)
      .send({
          password: 'Wrong Pwd'
      })
      .expect(403)
  })
  it('should return 403 if there is wrong id', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .delete('/api/users/' + 999999)
      .send({
          password: user1.password
      })
      .expect(403)
  })
})