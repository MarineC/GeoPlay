/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('POST /api/users/:userId/logout', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return username and id of new user when logout', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    return agent
      .post('/api/users/' + user.id_user + '/logout')
      .expect(200)
      .expect('Content-Type', /json/)
  })
  it('should return 403 error', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    const user2 = await Pre.createUser()
    return agent
      .post('/api/users/' + user2.id_user + '/logout')
      .expect(403)
      .expect('Content-Type', /json/)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUser()
    return agent
      .post('/api/users/' + user.id_user + '/logout')
      .expect(401)
  })
})