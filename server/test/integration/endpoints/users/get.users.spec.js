/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('GET /api/users/all', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return all existing users', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    const user2 = await Pre.createUser()
    return agent
      .get('/api/users/all')
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body)
          .to.be.an('array')
          .of.length(2)
        expect(response.body[0].id_user).to.equal(user1.id_user)
        expect(response.body[1].id_user).to.equal(user2.id_user)
      })
  })
  it('should return 401', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUser()
    const user2 = await Pre.createUser()
    return agent
      .get('/api/users/all')
      .expect(401)
  })
})