/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('POST /api/users/login', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return username and id of new user when login', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUser()
    return agent
      .post('/api/users/login')
      .send({
          username: user.username,
          password: user.password
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_user).to.equal(user.id_user)
        expect(response.body.username).to.equal(user.username)
      })
  })
  it('should return error if wrong credential', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUser()
    return agent
      .post('/api/users/login')
      .send({
          username: user.username,
          password: 'wrong pwd'
      })
      .expect(500)
  })
})