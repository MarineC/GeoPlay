/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'

describe('GET /api/users/:id/quizzes', () => {

  before(async () => {
      await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return all quizzes of user', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizzes(agent)
    return agent
      .get('/api/users/' + user1.id_user + '/quizzes')
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(2)
        expect(response.body[0].name).to.equal(user1.quizzes[0].name)
        expect(response.body[1].name).to.equal(user1.quizzes[1].name)
      })
  })
  it('should return 404 error', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .get('/api/users/' + 99999 + '/quizzes')
      .expect(404)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuizzes()
    return agent
      .get('/api/users/' + user1.id_user + '/quizzes')
      .expect(401)
  })
})