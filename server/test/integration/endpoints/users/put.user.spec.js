/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('PUT /api/users/:id/:newUsername', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should modify username and respond its id and new username', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    const newUsername = 'new_username'
    return agent
      .put('/api/users/' + user.id_user + '/' + newUsername)
      .send({ password: user.password })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_user).to.equal(user.id_user)
        expect(response.body.username).to.equal(newUsername)
      })
  })
  it('should send This username is already taken if the new username already exist', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    const user2 = await Pre.createUser()
    return agent
      .put('/api/users/' + user.id_user + '/' + user2.username)
      .send({ password: user.password })
      .expect(409)
      .then((response) => {
        expect(response.body).to.equal('This username is already taken')
      })
  })
  it('should send 401 if the user give the wrong password', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    const newUsername = 'new_username'
    return agent
      .put('/api/users/' + user.id_user + '/' + newUsername)
      .send({ password: 'WrongPwd' })
      .expect(401)
  })
  it('should send 401 when user isn\'t logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUser()
    const newUsername = 'new_username'
    return agent
    .put('/api/users/' + user.id_user + '/' + newUsername)
    .send({ password: user.password })
    .expect(401)
  })
  it('should send 403 if you try to update another user', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    const user2 = await Pre.createUser()
    const newUsername = 'new_username'
    return agent
    .put('/api/users/' + user2.id_user + '/' + newUsername)
    .send({ password: user.password })
    .expect(403)
  })
})

describe('PUT /api/users/:userId', () => {

    before(async () => {
      await Pre.clearDB()
    })
  
    after (() => postgresStore.close())
  
    it('should modify password and respond its id and username', async () => {
      const agent = await Pre.createExpress()
      const user = await Pre.createUserLogged(agent)
      const newPwd = 'new_pwd'
      return agent
        .put('/api/users/' + user.id_user)
        .send({ 
            currentPassword: user.password,
            newPassword: newPwd
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .then((response) => {
          expect(response.body.id_user).to.equal(user.id_user)
          expect(response.body.username).to.equal(user.username)
        })
    })
    it('should send 401 if the current password is false', async () => {
        const agent = await Pre.createExpress()
        const user = await Pre.createUserLogged(agent)
        const newPwd = 'new_pwd'
        return agent
        .put('/api/users/' + user.id_user)
        .send({ 
            currentPassword: 'wrong password',
            newPassword: newPwd
        })
        .expect(401)
      })
      it('should return 403 error if there is wrong id', async () => {
        const agent = await Pre.createExpress()
        const user1 = await Pre.createUserLogged(agent)
        const newUsername = 'new_username'
        return agent
          .put('/api/users/' + 99999 + '/' + newUsername)
          .send({ 
            password: user1.password
          })
          .expect(403)
      })
      it('should return 401 error if user isn\'t logged', async () => {
        const agent = await Pre.createExpress()
        const user = await Pre.createUser()
        const newPwd = 'new_pwd'
        return agent
          .put('/api/users/' + user.id_user)
          .send({ 
              currentPassword: user.password,
              newPassword: newPwd
          })
          .expect(401)
      })
  })