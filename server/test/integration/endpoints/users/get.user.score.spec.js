/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'

describe('GET /api/users/:id/scores', () => {

  before(async () => {
      await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return all score of user', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizAndScore(agent)
    return agent
      .get('/api/users/' + user1.id_user + '/scores')
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].score_value).to.equal(user1.scores.score_value)
      })
  })
  it('should return 404 error', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLogged(agent)
    return agent
      .get('/api/users/' + 99999 + '/scores')
      .expect(403)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuizAndScore()
    return agent
      .get('/api/users/' + user1.id_user + '/scores')
      .expect(401)
  })
})