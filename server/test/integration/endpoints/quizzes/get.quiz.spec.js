/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('GET /api/quizzes/:quizId', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return all quiz information', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .get('/api/quizzes/' + user1.quizzes.id_quiz)
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_quiz).to.equal(user1.quizzes.id_quiz)
        expect(response.body.name).to.equal(user1.quizzes.name)
      })
  })
  it('should return 404 error', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .get('/api/quizzes/' + 99999)
      .expect(404)
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuiz()
    return agent
      .get('/api/quizzes/' + user1.quizzes.id_quiz)
      .expect(401)
  })
})