/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('PUT /api/quizzes/:quizId', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())
  it('should modify quiz and return the new quiz informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuiz(agent)
    const newName = 'newName'
    const newDescription = 'newDescription'
    const newMapId = 'newMapId'
    const newDifficulty = 'newDifficulty'
    const newDuration = 26
    const newNbQuestion = 6
    return agent
      .put('/api/quizzes/' + user.quizzes.id_quiz)
      .send({
        newName: newName,
        newDescription: newDescription,
        newMapId: newMapId,
        newDifficulty: newDifficulty,
        newDuration: newDuration,
        newNbQuestion: newNbQuestion
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.name).to.equal(newName)
      })
  })
  it('should return "This quiz name is already taken" if the quiz name already exist', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuizzes(agent)
    return agent
      .put('/api/quizzes/' + user.quizzes[1].id_quiz)
      .send({
        newName: user.quizzes[0].name,
        newDescription: user.quizzes[0].description,
        newMapId: user.quizzes[0].mapid,
        newDifficulty: user.quizzes[0].difficulty,
        newDuration: user.quizzes[0].duration,
        newNbQuestion: user.quizzes[0].nb_questions
      })
      .expect(409)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.equal('This quiz name is already taken')
      })
  })
  it('should modify quiz and return the new quiz informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .put('/api/quizzes/' + user.quizzes.id_quiz)
      .send({
        newName: null,
        newDescription: user.quizzes.description,
        newMapId: user.quizzes.mapid,
        newDifficulty: user.quizzes.difficulty,
        newDuration: user.quizzes.duration,
        newNbQuestion: user.quizzes.nb_questions
      })
      .expect(400)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.equal('There is wrong argument to create a quiz')
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserWithQuiz()
    const newName = 'newName'
    const newDescription = 'newDescription'
    const newMapId = 'newMapId'
    const newDifficulty = 'newDifficulty'
    const newDuration = 26
    const newNbQuestion = 6
    return agent
      .put('/api/quizzes/' + user.quizzes.id_quiz)
      .send({
        newName: newName,
        newDescription: newDescription,
        newMapId: newMapId,
        newDifficulty: newDifficulty,
        newDuration: newDuration,
        newNbQuestion: newNbQuestion
      })
      .expect(401)
  })
})