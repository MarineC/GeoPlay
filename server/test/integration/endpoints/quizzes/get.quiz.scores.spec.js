/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('GET /api/quizzes/:quizId/scores', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return all scores for one quiz', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuiz(agent)
    const user2 = await Pre.createUser()
    user1.score = await Pre.createScore(user1.quizzes.id_quiz, user1.id_user)
    user2.score = await Pre.createScore(user1.quizzes.id_quiz, user2.id_user)
    return agent
      .get('/api/quizzes/' + user1.quizzes.id_quiz + '/scores')
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(2)
        expect(response.body[0].username).to.equal(user1.username)
        expect(response.body[1].username).to.equal(user2.username)
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuiz(agent)
    const user2 = await Pre.createUser()
    user1.score = await Pre.createScore(user1.quizzes.id_quiz, user1.id_user)
    user2.score = await Pre.createScore(user1.quizzes.id_quiz, user2.id_user)
    return agent
      .get('/api/quizzes/' + user1.quizzes.id_quiz + '/scores')
      .expect(401)
  })
})