/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('POST /api/quizzes/add', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return quiz informations', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    return agent
      .post('/api/quizzes/add')
      .send({
        name: 'Fake Quiz',
        description: 'Fake Description',
        mapid: 'Fake mapId',
        difficulty: 'Fake difficulty',
        duration: 25,
        id_user: user.id_user,
        nb_questions: 5
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body.id_quiz).to.be.a('number')
        expect(response.body.name).is.equal('Fake Quiz')
      })
  })
  it('should an return error message "A quizz with this name already exists" if the quiz name already exist', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .post('/api/quizzes/add')
      .send({
        name: user.quizzes.name,
        description: 'Fake Description',
        mapid: 'Fake mapId',
        difficulty: 'Fake difficulty',
        duration: 25,
        id_user: user.id_user,
        nb_questions: 5
      })
      .expect(409)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.equal('A quizz with this name already exists')
      })
  })
  it('should return an error message with to prevent missing argument', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUserLogged(agent)
    return agent
      .post('/api/quizzes/add')
      .send({
          description: 'Creating quiz with missging argument'
      })
      .expect(400)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.equal('There is missing argument to create a quiz')
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user = await Pre.createUser()
    return agent
      .post('/api/quizzes/add')
      .send({
        name: 'Fake Quiz',
        description: 'Fake Description',
        mapid: 'Fake mapId',
        difficulty: 'Fake difficulty',
        duration: 25,
        id_user: user.id_user,
        nb_questions: 5
      })
      .expect(401)
  })
})