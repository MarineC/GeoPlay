/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'


describe('GET /api/quizzes/all', () => {

  before(async () => {
    await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return all existing quizzes', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuizzes(agent)
    const user2 = await Pre.createUserWithQuizzes()
    return agent
      .get('/api/quizzes/all')
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body).to.be.an('array').of.length(4)
        expect(response.body[0].name).to.equal(user1.quizzes[0].name)
        expect(response.body[1].name).to.equal(user1.quizzes[1].name)
        expect(response.body[2].name).to.equal(user2.quizzes[0].name)
        expect(response.body[3].name).to.equal(user2.quizzes[1].name)
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuizzes(agent)
    const user2 = await Pre.createUserWithQuizzes()
    return agent
      .get('/api/quizzes/all')
      .expect(401)
  })
})