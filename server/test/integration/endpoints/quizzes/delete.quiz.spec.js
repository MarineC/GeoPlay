/* eslint-disable no-unused-expressions */
import { expect } from 'chai'
const Pre = require('../../pre.js')
import postgresStore from '../../../../postgres-store.js'

describe('DELETE /api/quizzes/:quizId', () => {

  before(async () => {
      await Pre.clearDB()
  })

  after (() => postgresStore.close())

  it('should return informations of deleted quiz', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserLoggedWithQuiz(agent)
    return agent
      .delete('/api/quizzes/' + user1.quizzes.id_quiz)
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response) => {
        expect(response.body[0].id_quiz).to.equal(user1.quizzes.id_quiz)
        expect(response.body[0].name).to.equal(user1.quizzes.name)
      })
  })
  it('should return 401 error if not logged', async () => {
    const agent = await Pre.createExpress()
    const user1 = await Pre.createUserWithQuiz()
    return agent
      .delete('/api/quizzes/' + user1.quizzes.id_quiz)
      .expect(401)
  })
})