import Question from '../../models/question.model.js'

export default async function deleteQuestion (req, res) {
  const id = req.params.questionId
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const question = await Question.deleteQuestion(id)
  if (!question[0]) {
    res.status(404).json(question)
  } else {
    res.status(200).json(question)
  }
}
