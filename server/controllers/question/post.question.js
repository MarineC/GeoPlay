import Question from '../../models/question.model.js'
import Quiz from '../../models/quiz.model.js'

export async function postAddQuestions (req, res) {
  const quizId = req.body.quizId
  const questionList = req.body.questionList
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }

  const q = await Quiz.getQuiz(quizId)
  if(q === undefined)
    res.status(404).json({errorMessage: 'this quizz does not exists'})
  if (req.session.authUser.id === q.id_user) {
    const questions = await Question.addQuestions(quizId, questionList)
    if (!(questions[0])) {
      res.status(500).json(questions)
    } else {
      res.status(200).json(questions)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }

  
}
