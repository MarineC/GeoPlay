import Question from '../../models/question.model.js'

export async function getResponseLocationId (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const questionId = req.params.questionId
  const rep = await Question.getResponse(questionId)
  if (!rep) {
    res.status(400).json(rep)
  } else {
    res.status(200).json(rep)
  }
}
