import Quiz from '../../models/quiz.model.js'

export default async function deleteQuiz (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }

  const id = req.params.quizId
  const q = await Quiz.getQuiz(id)
  if (req.session.authUser.id === parseInt(q.id_user)) {
    const quiz = await Quiz.deleteQuiz(id)
    if (!quiz[0]) {
      res.status(404).json(quiz)
    } else {
      res.status(200).json(quiz)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }
}
