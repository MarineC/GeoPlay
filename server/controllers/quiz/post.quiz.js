import Quiz from '../../models/quiz.model.js'

export async function postAddQuiz (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const quiz = await Quiz.addQuiz(
    req.body.name,
    req.body.description,
    req.body.mapid,
    req.body.difficulty,
    req.body.duration,
    req.body.id_user,
    req.body.nb_questions
  )
  if (!(quiz.name)) {
    if (quiz.message.includes('rompt la contrainte unique « quizzes_name_key »')) { 
      quiz.message = 'A quizz with this name already exists'
      res.status(409).json(quiz.message)
    } else {
      quiz.message = 'There is missing argument to create a quiz'
      res.status(400).json(quiz.message)
    }
  } else {
    res.status(200).json(quiz)
  }
}
