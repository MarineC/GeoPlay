import Quiz from '../../models/quiz.model.js'

export async function putModifyQuiz (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const quizId = req.params.quizId
  const q = await Quiz.getQuiz(quizId)
  if (req.session.authUser.id === q.id_user) {
    const newName = req.body.newName
    const newDescription = req.body.newDescription
    const newMapId = req.body.newMapId
    const newDifficulty = req.body.newDifficulty
    const newDuration = req.body.newDuration
    const newNbQuestion = req.body.newNbQuestion
    const quiz = await Quiz.updateQuiz(quizId, newName, newDescription, newMapId, newDifficulty, newDuration, newNbQuestion)
    if (quiz === undefined) {
      res.status(404).json()
    }
    if (!(quiz.name)) {
      if (quiz.message && quiz.message.includes('rompt la contrainte unique « quizzes_name_key »')) {
        quiz.message = 'This quiz name is already taken'
        res.status(409).json(quiz.message)
      } else if (quiz.message && quiz.message.includes('une valeur NULL viole la contrainte NOT NULL')) {
        quiz.message = 'There is wrong argument to create a quiz'
        res.status(400).json(quiz.message)
      }
    } else {
      res.status(200).json(quiz)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }

  
}
