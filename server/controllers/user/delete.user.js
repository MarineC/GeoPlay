import User from '../../models/user.model.js'

export default async function deleteUser (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }

  const id = req.params.userId
  const password = req.body.password
  if (req.session.authUser.id === parseInt(id)) {
   const user = await User.deleteUser(id, password)
    if (!user) {
      res.status(403).json(user)
    } else {
      delete req.session.authUser
      res.status(200).json(user)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }
}
