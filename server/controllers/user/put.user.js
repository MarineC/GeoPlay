import User from '../../models/user.model.js'

export async function putModifyPassword (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const id = req.params.userId
  const currentPassword = req.body.currentPassword
  const newPassword = req.body.newPassword

  if (req.session.authUser.id === parseInt(id)) {
    const user = await User.updateUserpassword(id, currentPassword, newPassword)
    if (!user) {
      res.status(401).json(user)
    } else {
      res.status(200).json(user)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }
  
}

export async function putModifyUsername (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const id = req.params.userId
  const username = req.params.newUsername
  const password = req.body.password
  if (req.session.authUser.id === parseInt(id)) {
    const user = await User.updateUsername(id, username, password)
    if (user === undefined) {
      const message = 'Not authorized'
      res.status(401).json(message)
    }
    if (!(user.username)) {
      if (user.message.includes('rompt la contrainte unique « users_username_key »')) { 
        user.message = 'This username is already taken'
        res.status(409).json(user.message)
      }
    } else {
      res.status(200).json(user)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }
}
