import User from '../../models/user.model.js'

export async function postAddUser (req, res) {
  const user = await User.addUser(req.body.username, req.body.password)
  if (!(user.username)) {
    if (user.message.includes('rompt la contrainte unique « users_username_key »')) { user.message = 'This username is already taken' }
    res.status(409).json(user.message)
  } else {
    res.status(200).json(user)
  }
}

export async function login (req, res) {
  const user = await User.checkUserCreds(req.body.username, req.body.password)
  if (!user) {
    res.status(500).json(user)
  } else {
    req.session.authUser = { id: user.id_user }
    res.status(200).json(user)
  }
}

export async function logout (req, res) {
  const userId = req.params.userId
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  if (req.session.authUser.id === parseInt(userId)) {
    delete req.session.authUser
    res.status(200).json()
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }
}