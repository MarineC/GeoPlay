import Score from '../../models/score.model.js'

export default async function deleteScore (req, res) {
  const id = req.params.scoreId
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const score = await Score.deleteScore(id)
  if (!score) {
    res.status(404).json(score)
  } else {
    res.status(200).json(score)
  }
}
