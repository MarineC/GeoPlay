import Score from '../../models/score.model.js'

export async function getScoreUser (req, res) {
  if(req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
    return
  }
  const userId = req.params.userId
  if (req.session.authUser.id === parseInt(userId)) {
    const scores = await Score.getScoreUser(userId)
    if (!scores[0]) {
      res.status(404).json(scores)
    } else {
      res.status(200).json(scores)
    }
  } else {
    res.status(403).json({errorMessage: 'not authorized'})
  }
}

export async function getScoreQuiz (req, res) {
  if(req.session.authUser === {} || req.session.authUser === undefined){
    res.status(401).json({errorMessage: 'not authenticated'})
  }else{
    const quizId = req.params.quizId
    const scores = await Score.getScoreQuiz(quizId)
    if (!scores) {
      res.status(400).json(scores)
    } else {
      res.status(200).json(scores)
    }
  }
}