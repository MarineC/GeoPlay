import { Router } from 'express'
import { getQuizzes, getQuiz } from '../controllers/quiz/get.quiz.js'
import { getScoreQuiz } from '../controllers/score/get.score.js'
import { postAddQuiz } from '../controllers/quiz/post.quiz.js'
import { putModifyQuiz } from '../controllers/quiz/put.quiz.js'
import deleteQuiz from '../controllers/quiz/delete.quiz.js'

const quiz = Router()
quiz.get('/all', getQuizzes)
quiz.get('/:quizId', getQuiz)
quiz.get('/:quizId/scores', getScoreQuiz)
quiz.post('/add', postAddQuiz)
quiz.put('/update/:quizId', putModifyQuiz)
quiz.delete('/delete/:quizId', deleteQuiz)

export default quiz
