import { Router } from 'express'
import { getUsers, getUser } from '../controllers/user/get.user.js'
import { postAddUser, login, logout } from '../controllers/user/post.user.js'
import { putModifyPassword, putModifyUsername } from '../controllers/user/put.user.js'
import deleteUser from '../controllers/user/delete.user.js'
import { getScoreUser } from '../controllers/score/get.score.js'
import { getQuizUser } from '../controllers/quiz/get.quiz.js'

const user = Router()
user.get('/all', getUsers)
user.get('/:userId', getUser)
user.get('/:userId/quizzes', getQuizUser)
user.get('/:userId/scores', getScoreUser)
user.post('/add', postAddUser)
user.post('/login', login)
user.post('/:userId/logout', logout)
user.put('/:userId', putModifyPassword)
user.put('/:userId/:newUsername', putModifyUsername)
user.delete('/:userId', deleteUser)

export default user
