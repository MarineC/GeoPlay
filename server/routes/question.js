import { Router } from 'express'
import { getResponseLocationId } from '../controllers/question/get.question.js'
import { postAddQuestions } from '../controllers/question/post.question.js'
import { putModifyQuestion } from '../controllers/question/put.question.js'
import deleteQuestion from '../controllers/question/delete.question.js'

const question = Router()
question.get('/response/:questionId', getResponseLocationId)
question.post('/add', postAddQuestions)
question.put('/update/:questionId', putModifyQuestion)
question.delete('/delete/:questionId', deleteQuestion)

export default question
