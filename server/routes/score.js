import { Router } from 'express'
import { getScoreQuiz } from '../controllers/score/get.score.js'
import { postAddScore } from '../controllers/score/post.score.js'
import { putModifyScore } from '../controllers/score/put.score.js'
import deleteScore from '../controllers/score/delete.score.js'

const score = Router()
score.get('/:quizId', getScoreQuiz)
score.post('/add', postAddScore)
score.put('/:scoreId', putModifyScore)
score.delete('/:scoreId', deleteScore)

export default score
