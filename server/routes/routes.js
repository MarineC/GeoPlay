import express from 'express'
import userRouter from './user.js'
import quizRouter from './quiz.js'
import questionRouter from './question.js'
import scoreRouter from './score.js'

const session = require('express-session')
const routes = express.Router()
routes.use(
  session({
    secret: 'super-secret-key',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 }
  })
)
routes.use('/users', userRouter)
routes.use('/quizzes', quizRouter)
routes.use('/questions', questionRouter)
routes.use('/scores', scoreRouter)

export default routes
