import express from 'express'
import session from 'express-session'
import logger from 'morgan'
import apiRouter from './routes/routes.js'
import config from './server.config.js'
import postgresStore from './postgres-store.js'

postgresStore.init(config.postgres)

const server = express()
server.disable('x-powered-by')

server.use(logger('dev'))
server.use(session({
  secret: config.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}))
server.use(express.json())
server.use(express.urlencoded({ extended: false }))

server.use('/api/', apiRouter)

export default server
