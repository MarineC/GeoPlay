import postgresStore from './postgres-store.js'

class ManageDB {
    
    static async deleteTablesContent () {
          const result = await postgresStore.client.query(
            "SELECT tablename FROM pg_tables WHERE schemaname = 'public';"
          )
          for (const row of result.rows) {
            if (row.tablename !== 'spatial_ref_sys') {
              await postgresStore.client.query(`DELETE FROM "${row.tablename}"`)
            }
          }
        }
}

module.exports = ManageDB
